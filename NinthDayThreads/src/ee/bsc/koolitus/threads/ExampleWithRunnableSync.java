package ee.bsc.koolitus.threads;

public class ExampleWithRunnableSync implements Runnable {
	static int i = 0;

	@Override
	public void run() {
		synchronized (this) {
			i++;
			System.out.println(Thread.currentThread().getName() + " i = " + i);

		}

	}
}
