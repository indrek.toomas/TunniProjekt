package ee.bsc.koolitus.threads;

public class Main {
	public static void main(String[] args) {
		// ExampleWithThread thread1 = new ExampleWithThread();
		// thread1.setPriority(Thread.MIN_PRIORITY);
		// thread1.start();
		//
		// ExampleWithThread thread2 = new ExampleWithThread();
		// thread2.setPriority(Thread.MAX_PRIORITY);
		// thread2.start();

//		ExampleWithRunnable runnable1 = new ExampleWithRunnable();
//		Thread t1 = new Thread(runnable1);
//		ExampleWithRunnable runnable2 = new ExampleWithRunnable();
//		Thread t2 = new Thread(runnable2);
//
//		t1.start();
//		t2.start();
		
	ExampleWithRunnableSync syncRunnable = new ExampleWithRunnableSync();
	Thread t1 = new Thread(syncRunnable, "First thread");
	t1.start();
	Thread t2 = new Thread(syncRunnable, "Second thread");
	t2.start();
	Thread t3 = new Thread(syncRunnable, "Third thread");
	t3.start();
	
	}
}
