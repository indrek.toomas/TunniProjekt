import java.util.Arrays;

public class CitiesInArray {
	public static void main(String[] args) {

		String[][] tabel = { { " Helsinki ", " Helsingi ", " Helsinki ", " Country = Soome ", " Helsingfors " },
				{ " Tallinn ", " Tallinn ", " Tallinn ", " Country = Eesti ", "" },
				{ " Riga", " Riia ", " Riga ", " Country = Läti ", "", },
				{ " Moscow ", " Moskva ", " Москва ", " Country = Venemaa ", "" } };

		for (int rida = 0; rida < tabel.length; rida++) {
			for (int veerg = 0; veerg < tabel[rida].length; veerg++) {
				System.out.print(tabel[rida][veerg]);
			}
			System.out.println();

		}

		System.out.println(" ");

		for (int rida = 0; rida < tabel.length; rida++) {
			System.out.println("" + tabel[rida][3] + "; pealinn -" + tabel[rida][1] + "; inglise keeles -"
					+ tabel[rida][0] + "; kohalikus keeles -" + tabel[rida][2] + "" + tabel[rida][4]);

		}

		System.out.println(" ");

		String[][] tabel2 = { { " Helsinki ", " Helsingi ", " Helsinki ", " Soome ", " Helsingfors " },
				{ " Tallinn ", " Tallinn ", " Tallinn ", " Eesti ", "" },
				{ " Riga", " Riia ", " Riga ", " Läti ", "", },
				{ " Moscow ", " Moskva ", " Москва ", " Venemaa ", "" } };

		for (int rida = 0; rida < tabel2.length; rida++) {
			System.out.println(" Riik -" + tabel2[rida][3] + "; pealinn -" + tabel2[rida][1] + "; inglise keeles -"
					+ tabel2[rida][0] + "; kohalikus keeles -" + tabel2[rida][2]);

		}

		System.out.println(" ");

		String[][] tabel1 = { { " Helsinki ", " Helsingi ", " Helsinki ", " Country = Soome " },
				{ " Tallinn ", " Tallinn ", " Tallinn ", " Country = Eesti " },
				{ " Riga", " Riia ", " Riga ", " Country = Läti " }, { " LongStrocking City ", " Pikksuka linn ",
						" LangStrump ", " Country = Kurrunurruvutisaare Kuningriik " } };

		for (int rida = 0; rida < tabel1.length; rida++) {
			System.out.println("" + tabel1[rida][3] + "; pealinn -" + tabel1[rida][1] + "; inglise keeles -"
					+ tabel1[rida][0] + "; kohalikus keeles -" + tabel1[rida][2]);

		}

		System.out.println(" ");

		// deleting elements

		String[][] tabel4 = { { " Helsinki ", " Helsingi ", " Helsinki ", " Country = Soome " },
				{ " Tallinn ", " Tallinn ", " Tallinn ", " Country = Eesti " },
				{ " Riga", " Riia ", " Riga ", " Country = Läti " }, { " LongStrocking City ", " Pikksuka linn ",
						" LangStrump ", " Country = Kurrunurruvutisaare Kuningriik " } };
		tabel4[1] = tabel4[tabel4.length - 1];
		String[] newTabel = new String[tabel4.length - 1];

		for (int i = 0; i < newTabel.length; i++) {
			newTabel[i] = tabel4[i][i];
		}

		System.out.println(Arrays.toString(newTabel));

	}
}
