package ee.bcs.koolitus.abstractclass;

public class Triangle extends Shape {

	private double sideALength = 0;
	private double sideBLength = 0;
	private double sideCLength = 0;

	@Override
	public double getPerimeter() {
		return (sideALength + sideBLength + sideCLength);
	}

	public double getSideALength() {
		return sideALength;
	}

	public void setSideALength(double sideALength) {
		this.sideALength = sideALength;
	}

	public double getSideBLength() {
		return sideBLength;
	}

	public void setSideBLength(double sideBLength) {
		this.sideBLength = sideBLength;
	}

	public double getSideCLength() {
		return sideCLength;
	}

	public void setSideCLength(double sideCLength) {
		this.sideCLength = sideCLength;
	}

}
