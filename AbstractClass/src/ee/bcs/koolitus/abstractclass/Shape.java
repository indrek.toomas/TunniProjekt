package ee.bcs.koolitus.abstractclass;

import java.awt.Point;

public abstract class Shape { // abstraktne klass
	public final String DEFAULT_COLOR = "green"; // konstant
	private Point centrePoint = new Point();

	public void setCentreCoordinates(int x, int y) { // tavaline meetod
		centrePoint.setLocation(x, y);

	}

	public abstract double getPerimeter();

	public Point getCentrePoint() {
		return centrePoint;
	}

	public void setCentrePoint(Point centrePoint) {
		this.centrePoint = centrePoint;
	}

}