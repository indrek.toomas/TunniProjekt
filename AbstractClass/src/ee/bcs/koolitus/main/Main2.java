package ee.bcs.koolitus.main;

import ee.bcs.koolitus.abstractclass.Kolmnurk;
import ee.bcs.koolitus.abstractclass.IKujund;

public class Main2 {

	public static void main(String[] args) {
		Kolmnurk kolmnurk = new Kolmnurk();
		kolmnurk.setCentreCoordinates(2, 3);
		System.out.println(kolmnurk.getCentrePoint());
	}

}
