package ee.bcs.koolitus.main;

import ee.bcs.koolitus.abstractclass.Shape;
import ee.bcs.koolitus.abstractclass.Triangle;

public class Main {

	public static void main(String[] args) {
		Triangle triangle = new Triangle();

		System.out.println(triangle.DEFAULT_COLOR);

		triangle.setCentreCoordinates(1, 5);
		System.out.println(triangle.getCentrePoint().toString());

		triangle.setSideALength(4);
		triangle.setSideBLength(6);
		triangle.setSideCLength(7);

		System.out.println(triangle.getPerimeter());

	}
}
