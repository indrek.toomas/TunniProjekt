package ee.bcs.koolitus;

/**
 * See klass on andmetyypide tutvustamiseks
 * 
 * @author opilane
 * @version 0,1
 */
public class Andmetyybid {

	public static void main(String[] args) {
		byte byteTyyp = 1;
		System.out.println(byteTyyp);
		byte byteTyyp2 = 12;
		System.out.println(byteTyyp2);

		byte kolmasByte = (byte) (byteTyyp + byteTyyp2);

		System.out.println(kolmasByte);
		System.out.println(byteTyyp + byteTyyp2);

		short shortTyyp = 2;
		short shortTyyp2 = 4;
		System.out.println(shortTyyp + shortTyyp2);

		short test = (short) (byteTyyp2 + shortTyyp2);
		System.out.println(byteTyyp2 + shortTyyp2);

		int vanus = 29;

		int synniAasta = 1987;

		int arvutatudVanus = 2017 - synniAasta;

		System.out.println(arvutatudVanus);

		double komagaArv = 10.7;
		Double komagaArv2 = 10.7;
		System.out.println(komagaArv);
		System.out.println(komagaArv2.compareTo(Double.valueOf("10.7")));
		System.out.println(komagaArv + 15.8);

		char mark = 'A';
		char mark2 = 'B';
		System.out.println(mark + mark2);

		String s1 = "A";
		String s2 = "B";
		System.out.println(s1 + s2);

		int a = 3;
		int d = ++a;
		System.out.println("a = " + a + " ja d = " + d);

		int a1 = 1;
		int b1 = 1;
		int c1 = 3;
		System.out.println(a1 == b1);
		System.out.println(a1 == c1);

		a1 = c1;
		System.out.println(a1 == b1);
		System.out.println(a1 == c1);

		int x1 = 10;
		int x2 = 20;
		int y1 = ++x1;
		/* Siin võtab y1 x1+1 väärtuse */
		System.out.println(x1);
		System.out.println(y1);

		int y2 = x2++;
		/* Siin võtab y2 x2+1 väärtuse */
		System.out.println(x2);
		System.out.println(y2);

		int a2 = 18 % 3;
		int b2 = 19 % 3;
		int c2 = 20 % 3;
		int d2 = 21 % 3;
		System.out.println(a2 + " " + b2 + " " + c2 + " " + d2);
		System.out.println(a2);
		System.out.println(b2);
		System.out.println(c2);
		System.out.println(d2);

		String eesnimi = "Indrek";
		String perenimi = "Toomas";

		String taisNimi = "Indrek toomas";
		String taisNimi2 = eesnimi + " " + perenimi;

		System.out.println(taisNimi == taisNimi2);
		System.out.println(taisNimi.equals(taisNimi2));
		System.out.println(taisNimi.equalsIgnoreCase(taisNimi2));

	}

}
