package ee.bcs.koolitus.vaiksem;

import ee.bcs.koolitus.nahtavus.Nahtavus;

public class NahtavuseAlamklass extends Nahtavus {
	Nahtavus nahtavus = new Nahtavus();

	public void tetiNahtavuseMuutujaid() {
//		System.out.println(nahtavus.klassiMuutujaPrivate);
//		System.out.println(nahtavus.klassiMuutujaDefault);
		System.out.println(this.klassiMuutujaProtected);
		System.out.println(nahtavus.klassiMuutujaPublic);

	}

}
