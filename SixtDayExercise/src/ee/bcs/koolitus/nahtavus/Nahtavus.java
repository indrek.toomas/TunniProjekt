package ee.bcs.koolitus.nahtavus;

public class Nahtavus {
	private String klassiMuutujaPrivate = "Private klassi muutuja on nähtav ainult klassis";
	String klassiMuutujaDefault = "Default klassi muutuja on nähtav ainult sama kausta ee.bcs.koolitus.nahtavus piires";
	protected String klassiMuutujaProtected = "Protected klassi muutuja on nähtav ka teises kaustas ee.bcs.koolitus.nahtavus.vaiksem olevale alamklassile NahtavuseAlamklass";
	public String klassiMuutujaPublic = "Public klassi muutuja on nähtav ainult kogu projekti ulatuses";

	public String getKlassiMuutujaPrivate() {
		return klassiMuutujaPrivate;
	}

	public void setKlassiMuutujaPrivate(String klassiMuutujaPrivate) {
		this.klassiMuutujaPrivate = klassiMuutujaPrivate;
	}

	public String getKlassiMuutujaDefault() {
		return klassiMuutujaDefault;
	}

	public void setKlassiMuutujaDefault(String klassiMuutujaDefault) {
		this.klassiMuutujaDefault = klassiMuutujaDefault;
	}

	public String getKlassiMuutujaProtected() {
		return klassiMuutujaProtected;
	}

	public void setKlassiMuutujaProtected(String klassiMuutujaProtected) {
		this.klassiMuutujaProtected = klassiMuutujaProtected;
	}

	public String getKlassiMuutujaPublic() {
		return klassiMuutujaPublic;
	}

	public void setKlassiMuutujaPublic(String klassiMuutujaPublic) {
		this.klassiMuutujaPublic = klassiMuutujaPublic;
	}

}
