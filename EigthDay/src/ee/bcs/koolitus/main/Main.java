package ee.bcs.koolitus.main;

import java.math.BigDecimal;

import ee.bcs.koolitus.eigthday.Employee;

public class Main {

	public void main(String[] args) {

		Employee employee1 = new Employee(BigDecimal.valueOf(1_000));
		BigDecimal salary = BigDecimal.TEN;

		System.out.println(employee1.getSalary());

		Employee employee2 = null;
		employee1.changeSalary(employee1, salary);

		changeSalary(employee1, BigDecimal.valueOf(50));

		Employee employeeWithName = new Employee("Mari", BigDecimal.valueOf(1200));
		System.out.println(employeeWithName.getName() + " Has Salary " + employeeWithName.getSalary());
		System.out.println(employeeWithName);

	}

	public void changeSalary(Employee employee) {
		employee = new Employee();
		employee.setSalary(employee.getSalary().add(BigDecimal.valueOf(100)));
		System.out.println(employee.getSalary());

		changeSalary(employee, BigDecimal.valueOf(100));
	}

	public void changeSalary(Employee employee, BigDecimal newSalary) {
		employee.setSalary(employee.getSalary().add(newSalary));
		System.out.println(employee.getSalary().add(newSalary));

	}

}