package ee.bcs.koolitus.eigthday;

import java.math.BigDecimal;

public class Employee {
	public BigDecimal salary = BigDecimal.ZERO;
	public String name;

	public Employee() {

	}

	public Employee(BigDecimal salary) {
		this.salary = salary;

	}

	public Employee(String name, BigDecimal salary) {
		this.name = name;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public Employee setSalary(BigDecimal salary) {
		this.salary = salary;
		return this;

	}

	public String getName() {
		return name;
	}

	public Employee setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public String toString() {
		return "Employee " + name + " Has salary " + salary;
	}

	public void changeSalary() {
		changeSalary(BigDecimal.valueOf(100));
	}

	public void changeSalary(BigDecimal newSalary) {
		setSalary(getSalary().add(newSalary));

	}

	public void changeSalary(Employee employee1, BigDecimal salary2) {
		// TODO Auto-generated method stub

	}
}
