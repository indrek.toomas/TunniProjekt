package test.ee.bcs.koolitus.employee;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import ee.bcs.koolitus.eigthday.*;

import org.junit.Assert;
import org.junit.Test;

public class EmployeeTest {

	@Test
	public void testChangeSalaryUpdatesSalary() {
		Employee testEmployee = new Employee("Mari", BigDecimal.valueOf(1000));
		testEmployee.changeSalary();
		Assert.assertEquals(BigDecimal.valueOf(1200), testEmployee.getSalary());
		
	}
	public void testChangeSalaryUpdatesSalaryWithArgument() {
		Employee testEmployee = new Employee("Mari", BigDecimal.valueOf(1000));
		testEmployee.changeSalary(BigDecimal.valueOf(100));
		Assert.assertEquals(BigDecimal.valueOf(1200), testEmployee.getSalary());

}
}
