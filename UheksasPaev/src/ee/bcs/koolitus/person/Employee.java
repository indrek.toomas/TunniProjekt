package ee.bcs.koolitus.person;

import ee.bcs.koolitus.person.Person;

import java.math.BigDecimal;

public class Employee extends Person {

	public static final int MINIMUM_WORKING_AGE = 18;

	private BigDecimal salary;
	private String employeesName;

	private Employee() {
		super(MINIMUM_WORKING_AGE);

	}

	public Employee(int age, String employeesName, BigDecimal salary) {
		super(age);
		this.employeesName = employeesName;
		this.salary = salary;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public String getEmployeesName() {
		return employeesName;
	}

	public void setEmployeesName(String employeesName) {
		this.employeesName = employeesName;
	}

}
