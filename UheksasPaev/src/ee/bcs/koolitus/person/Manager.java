package ee.bcs.koolitus.person;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.ArrayList;

public class Manager extends Employee {
	String departement = new String();

	public Manager(int age, String employeesName, BigDecimal salary, String departement) {
		super(age, employeesName, salary);
		this.departement = departement;
	}

	List<Employee> departementStaff = new ArrayList<>();

	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public void addEmployeeToDepartement(Employee employee) {
		departementStaff.add(employee);
	}

	public List<Employee> getDepartementStaff() {
		return this.departementStaff;
	}
}