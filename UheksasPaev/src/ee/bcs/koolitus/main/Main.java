package ee.bcs.koolitus.main;

import ee.bcs.koolitus.person.Employee;
import ee.bcs.koolitus.person.Person;
import ee.bcs.koolitus.person.Manager;

import java.math.BigDecimal;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Person p = new Person(18);
		Employee employee = new Employee(20, "Mari", BigDecimal.valueOf(800));

		System.out.println(p.getAge());
		System.out.println(employee.getEmployeesName() + " is " + employee.getAge() + " years old and has salary "
				+ employee.getSalary());

		Manager manager = new Manager(35, "Liisa", BigDecimal.valueOf(2000), "accounting");

		System.out.println(manager.getEmployeesName() + " manager " + manager.getDepartement());

		Employee employee2 = new Employee(45, "Kristjan", BigDecimal.valueOf(1500));

		manager.addEmployeeToDepartement(employee);
		manager.addEmployeeToDepartement(employee2);

		System.out.println("In her departement there are:");

		List<Employee> managerDepartementEmployees = manager.getDepartementStaff();
		manager.getDepartementStaff();
		for (Employee emp : managerDepartementEmployees) {
			System.out.println(emp.getEmployeesName());
		}

		System.out.println(" p id = " + p.getId());
		System.out.println(employee.getEmployeesName() + " p id = " + employee.getId());
		System.out.println(manager.getEmployeesName() + " p id = " + manager.getId());
		System.out.println(employee2.getEmployeesName() + " p id = " + employee2.getId());
	}
}
