package ee.bcs.koolitus.main;

public class Main {

	public static void main(String[] args) {
		Inimene inimene = new Inimene("Mario", Gender.MALE);

		switch (inimene.sugu) {
		case FEMALE:
			System.out.println("See inimene on naine");
			break;
		case MALE:
			System.out.println("See inimene on mees");
			break;
		}
		
		System.out.println(Gender.FEMALE.getDescription());
	}

}
