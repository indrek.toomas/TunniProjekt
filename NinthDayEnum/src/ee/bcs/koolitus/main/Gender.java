package ee.bcs.koolitus.main;

public enum Gender {
	MALE("isane"), FEMALE("emane");

	private String description;

	private Gender(String description) {
		this.description = description;
	}
	
	public String getDescription(){
		return this.description;
	}
}
