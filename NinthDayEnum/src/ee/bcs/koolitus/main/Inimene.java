package ee.bcs.koolitus.main;

public class Inimene {
	String nimi;
	Gender sugu;
	
	public Inimene(String nimi, Gender sugu) {
		this.nimi = nimi;
		this.sugu = sugu;
	}

}
