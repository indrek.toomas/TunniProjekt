package fourthday;

import java.util.ArrayList;
import java.util.List;

public class Liides {

	public static void main(String[] args) {
		List<String> tekstiList = new ArrayList<>();
		tekstiList.add("This is first sentence");
		tekstiList.add("This is second sentence");
		printArray(tekstiList);

		tekstiList.add(1, "This is third sentence that is on second position");
		printArray(tekstiList);

		tekstiList.add("Fourth sentence goes to end of list");
		tekstiList.remove(1);
		printArray(tekstiList);

		System.out.println("List size is " + tekstiList.size());

		System.out.println("Text on second position is: '" + tekstiList.get(1) + "'");
		System.out.println("-------");

		List<Integer> integerListWithInitialSize = new ArrayList<>(5);
		System.out.println("List size is " + integerListWithInitialSize.size());
		integerListWithInitialSize.add(200);
		integerListWithInitialSize.add(300);
		System.out.println("List size is " + integerListWithInitialSize.size());
		for (Integer listElements : integerListWithInitialSize) {
			System.out.println(listElements);

		}
	}

	public static void printArray(List<String> tekstiList) {
		tekstiList.forEach(textElement -> {					//lambda meetod, vt ka alternatiivset varianti.
			System.out.println(textElement);
		});
		System.out.println("-------");
		
		/*Alternatiivne variant oleks:
		 * for(Integer textElement : tekstiList) {
		 */

	}

}
