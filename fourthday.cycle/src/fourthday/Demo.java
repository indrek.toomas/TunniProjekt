package fourthday;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

public class Demo {
	public static void main(String[] args) {
		Set<String> tekstiList = new TreeSet<>();
		int i = 0;
		while (i < 10) {
			tekstiList.add("Seti Objekt" + ThreadLocalRandom.current().nextInt(0, 20)); // teeb
																						// suvalised
																						// numbrid
																						// etteantud
																						// vahemikus
			i++;
		}
		for (String setiElement : tekstiList) { // vastuses ei pruugi tulla 10.
												// nr, nagu i´ga märgitud, sest
												// TreeSet esitab ainult
												// unikaalsed numbrid.
			System.out.println(setiElement);
		}

		// Integer Set, et numbrid oleksid järjekooras

		Set<Integer> numbriList = new TreeSet<>();
		int j = 0;
		while (j < 10) {
			numbriList.add(ThreadLocalRandom.current().nextInt(0, 30));
			j++;
			for (Integer number : numbriList)
				System.out.println(number);

		}
	}
}
