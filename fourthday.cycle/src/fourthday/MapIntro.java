package fourthday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapIntro {
	public static void main(String[] args) {
		Map<String, List<String>> departmentEmployees = new HashMap<>();
		List<String> devTeam = new ArrayList<>();
		devTeam.add("Mari Maasikas");
		devTeam.add("Jüri Kuusk");
		departmentEmployees.put("development", devTeam);

		List<String> financeTeam = new ArrayList<>();
		financeTeam.add("Kati Kivi");
		departmentEmployees.put("finance", financeTeam);

		List<String> adminTeam = new ArrayList<>();
		adminTeam.add("Mati Kadakas");
		departmentEmployees.put("admin", adminTeam);

		departmentEmployees.get("finance").add("Kadri Kaasik");

		for (String key : departmentEmployees.keySet()) {
			System.out.println("In department " + key + " there are working: ");
			for (String employee : departmentEmployees.get(key)) {
				System.out.print(employee + "; ");
			}
			System.out.println();

		}

		// Sama, aga Lambda näol

		departmentEmployees.forEach((department, employees) -> {
			System.out.println("In department " + department + " there are working: ");
			employees.forEach(employee -> {
				System.out.print(employee + "; ");
			});
			System.out.println();
		});

	}

}
