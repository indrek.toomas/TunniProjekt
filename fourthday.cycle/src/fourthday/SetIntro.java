package fourthday;

import java.util.Set;
import java.util.TreeSet;

public class SetIntro {
	public static void main(String[] args) {
		simpleSetExample();

	}

	public static void simpleSetExample() {
		Set<String> simpleSet = new TreeSet<>();
		simpleSet.add("text");
		simpleSet.add("alphabet");
		simpleSet.add("horse");
		simpleSet.add("text");
		simpleSet.add("vague");
		simpleSet.forEach(t -> {
			System.out.println(t);
		});
		System.out.println("Set size is " + simpleSet.size() + "\n");

	}

}
