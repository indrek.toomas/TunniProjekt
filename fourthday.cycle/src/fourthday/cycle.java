package fourthday;

import java.util.List;

public class cycle {

	public static void main(String[] args) {
		int i = 5;
		do {
			System.out.println(i++);
		} while (i < 10);

		// i=15;
		int j = 1;
		String kontrollSona = "";
		while (i <= 20 && j < 30 && !kontrollSona.equals("exit")) {
			System.out.println("While ütleb, et i " + i + " ja j = " + j);
			i = i + 3;
			j = j + 10;

			if (j > 15) {
				kontrollSona = "exit";
			}
		}

		String[][] tabel = { { "Mati", "Tallinn" }, { "Kati", "Pärnu" }, { "Mari", "Narva" }, { "Jüri", "Türi" } };
		// esimene for ridade lugemiseks; teine for veergude lugemiseks
		// (tegemist on kahemõõtmelise massiiviga, siis on for kaks korda)

		for (int rida = 0; rida < tabel.length; rida++) { // määrab ära tabeli
															// (rea) pikkuse ja
															// tsükli lõpu
			for (int veerg = 0; veerg < tabel[rida].length; veerg++) { // määrab
																		// ära
																		// veeru
																		// pikkuse
																		// rea
																		// positsiooni
																		// abil
																		// ja
																		// tsükli
																		// lõpu
				System.out.println(tabel[rida][veerg]);
			}
		}

		for (int rida = 0; rida < tabel.length; rida++) {
			System.out.println(tabel[rida][0] + " linnast " + tabel[rida][1]);
		}

		String sona = "swedbank";
		for (int taht = 0; taht < sona.length(); taht++) {
			System.out.println((taht + 1) + ". täht on '" + sona.charAt(taht) + "'");
		}

		System.out.println("------while swedbank----");

		int taheIndeks = 0;
		while (taheIndeks < sona.length()) {
			System.out.println((taheIndeks + 1) + ". täht on '" + sona.charAt(taheIndeks) + "'");
			taheIndeks++;

		}

	}

}

// while - sama, mis VBA´s loop- nt toimib kuni mingi tingimus on täidetud
// (true) ja muutub mittekehtivaks false)
