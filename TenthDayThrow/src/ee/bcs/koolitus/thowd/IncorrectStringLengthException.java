package ee.bcs.koolitus.thowd;

public class IncorrectStringLengthException extends Exception {

	public IncorrectStringLengthException() {
		super();
	}

	public IncorrectStringLengthException(String message) { // message annab vea
															// sõnumina
		super(message);
	}

}
