package ee.bcs.koolitus.thowd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;

public class MyThrowingClass {

	public void readMyLittleFile() throws IOException, IncorrectStringLengthException, FileNotFoundException {

		FileReader reader = new FileReader(new File("Hello.txt"));
		BufferedReader bfReader = new BufferedReader(reader);
		String minuRida;
		List<String> read = new ArrayList<>();
		int counter = 1;
		while ((minuRida = bfReader.readLine()) != null) {
			if (minuRida.length() >= 3) {
				read.add(minuRida);
			} else {
				throw new IncorrectStringLengthException("Failis oli liiga lühike rida");
			}

			System.out.println(counter + "." + minuRida);
			counter++;
		}
		bfReader.close();
		reader.close();
		System.out.println("Listis on ridu " + read.size());
	}
	
	 public void vanem() throws FileNotFoundException, IOException, IncorrectStringLengthException {
	 readMyLittleFile();
	 }
}
