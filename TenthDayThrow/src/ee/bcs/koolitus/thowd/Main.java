package ee.bcs.koolitus.thowd;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IncorrectStringLengthException {
		MyThrowingClass myClass = new MyThrowingClass();
		try {
			myClass.readMyLittleFile();
			 myClass.vanem();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
